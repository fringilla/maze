// source: https://www.cabinorange.com/blog-1/2020/9/20/blending-and-translating-two-textures-in-an-unlit-shader-in-unity
// with small modification


Shader "Unlit/BlendBackgroundsShader"
{
    Properties
    {
      _MainTex("Background One", 2D) = "defaulttexture" {}
      _SecondaryTex("Background Two", 2D) = "defaulttexture" {}
      _Blend("Blend Amount", Range(0.0,1.0)) = 0.0
      _Color("Main Color", Color) = (1, 1, 1, 1)
    }
        SubShader{
          Tags{}
          LOD 100

          Pass
          {
              CGPROGRAM
              #pragma vertex vert
              #pragma fragment frag
              #pragma multi_compile_fog

              #include "UnityCG.cginc"

              struct appdata
              {
                  float4 vertex : POSITION;
                  float2 uv : TEXCOORD0;
              };

              struct v2f
              {
                  float2 uv : TEXCOORD0;
                  float2 uv2 : TEXCOORD1;
                  UNITY_FOG_COORDS(2)
                  float4 vertex : SV_POSITION;
              };

              sampler2D _MainTex;
              float4 _MainTex_ST;
              sampler2D _SecondaryTex;
              float4 _SecondaryTex_ST;
              float _Blend;
              float4 _Color;

              
              v2f vert(appdata v)
              {
                  v2f o;
                  o.vertex = UnityObjectToClipPos(v.vertex);

                  o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                  o.uv2 = TRANSFORM_TEX(v.uv, _SecondaryTex);
                  UNITY_TRANSFER_FOG(o,o.vertex);

                  return o;
              }

              fixed4 frag(v2f i) : SV_Target
              {
                  // sample the texture
                  fixed4 col = lerp(tex2D(_MainTex, i.uv), tex2D(_SecondaryTex, i.uv2), _Blend);
              // apply fog
              UNITY_APPLY_FOG(i.fogCoord, col);
              return col * _Color;
          
              }
          ENDCG
      }
      }
}