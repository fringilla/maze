﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
public class Maze
{
    public MazeCell[,] grid;
    public Position startPosition;
    public Position exitPosition;
}
class BacktrackerCell
{
    public int posX, posY;
    public bool visited;
}

class NeighbourCell
{
    public BacktrackerCell cell;
    public EWallSide commonWall;
    public NeighbourCell(BacktrackerCell cell, EWallSide commonWall)
    {
        this.cell = cell;
        this.commonWall = commonWall;
    }
}
public static class MazeGenerator
{
    public static Maze GenerateMaze(int width, int height, float trapPropability, int seed = 0)
    {
        if (seed > 0)
        {
            Random.InitState(seed);
        }

        Maze maze = new Maze
        {
            grid = new MazeCell[width, height],
        };

        bool horizontal = Random.value > 0.5f;
        //start and end position on two opposite sides
        if (horizontal)
        {
            bool startPositionOntheLeft = Random.value > 0.5f;
            maze.startPosition = startPositionOntheLeft ? new Position(0, Random.Range(0, height)) : new Position(width - 1, Random.Range(0, height));
            maze.exitPosition = startPositionOntheLeft ? new Position(width - 1, Random.Range(0, height)) : new Position(0, Random.Range(0, height));
        }
        else
        {
            bool stratPositionOnTop = Random.value > 0.5f;
            maze.startPosition = stratPositionOnTop ? new Position(Random.Range(0, width), height - 1) : new Position(Random.Range(0, width), 0);
            maze.exitPosition = stratPositionOnTop ? new Position(Random.Range(0, width), 0) : new Position(Random.Range(0, width), height - 1);
        }

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                MazeCell cell = new MazeCell(true, true, true, true, x, y);
                maze.grid[x, y] = cell;
                if (!cell.IsStartpoint(maze.startPosition) && !cell.IsExit(maze.exitPosition))
                {
                    maze.grid[x, y].hasTrap = Random.value < trapPropability;
                }

            }
        }



        Position backtrackerInitialPoint = new Position(Random.Range(0, width), Random.Range(0, height));

        maze.grid = Backtracker(maze.grid, backtrackerInitialPoint);

        return maze;
    }

    public static MazeCell[,] Backtracker(MazeCell[,] grid, Position initialCell)
    {
        int width = grid.GetLength(1), height = grid.GetLength(0);
        var neighboursGrid = new BacktrackerCell[width, height];

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                neighboursGrid[x, y] = new BacktrackerCell
                {
                    posX = x,
                    posY = y,
                    visited = false
                };
            }
        }
        Stack<BacktrackerCell> visitedCells = new Stack<BacktrackerCell>();
        var initial = neighboursGrid[initialCell.posX, initialCell.posY];
        initial.visited = true;
        visitedCells.Push(initial);

        while (visitedCells.Count > 0)
        {
            var cell = visitedCells.Peek();
            var neighbours = GetUnvisitedNeighbours(neighboursGrid, cell);
            if (neighbours.Count > 0)
            {
                var index = Random.Range(0, neighbours.Count);
                var neighbour = neighbours[index];
                grid[neighbour.cell.posX, neighbour.cell.posY].RemoveOppositeWall(neighbour.commonWall);
                grid[cell.posX, cell.posY].RemoveWall(neighbour.commonWall);
                neighbour.cell.visited = true;
                visitedCells.Push(neighbour.cell);
                continue;

            }
            visitedCells.Pop();
        }
        return grid;
    }

    private static List<NeighbourCell> GetUnvisitedNeighbours(BacktrackerCell[,] grid, BacktrackerCell cell)
    {
        var neighbours = new List<NeighbourCell>();
        if (cell.posY - 1 >= 0)
        {
            var neighbour = new NeighbourCell(grid[cell.posX, cell.posY - 1], EWallSide.WALL_BOTTOM);
            if (!neighbour.cell.visited)
            {
                neighbours.Add(neighbour);
            }
        }
        if (cell.posY + 1 <= grid.GetLength(0) - 1)
        {
            var neighbour = new NeighbourCell(grid[cell.posX, cell.posY + 1], EWallSide.WALL_UP);
            if (!neighbour.cell.visited)
            {
                neighbours.Add(neighbour);
            }
        }
        if (cell.posX - 1 >= 0)
        {
            var neighbour = new NeighbourCell((grid[cell.posX - 1, cell.posY]), EWallSide.WALL_LEFT);
            if (!neighbour.cell.visited)
            {
                neighbours.Add(neighbour);
            }
        }
        if (cell.posX + 1 <= grid.GetLength(1) - 1)
        {
            var neighbour = new NeighbourCell(grid[cell.posX + 1, cell.posY], EWallSide.WALL_RIGHT);
            if (!neighbour.cell.visited)
            {
                neighbours.Add(neighbour);
            }
        }
        return neighbours;
    }
}