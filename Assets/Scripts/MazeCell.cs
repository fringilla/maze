﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public enum EWallSide
{
    WALL_LEFT,
    WALL_UP,
    WALL_RIGHT,
    WALL_BOTTOM
}
public struct Position
{
    public int posX, posY;
    public Position(int x, int y)
    {
        posX = x;
        posY = y;
    }
}

public class MazeCell
{
    public bool wallLeft;
    public bool wallUp;
    public bool wallRight;
    public bool wallBottom;
    public Position position;
    public bool hasTrap = false;

    public MazeCell(bool left, bool up, bool right, bool down, int x, int y)
    {
        wallLeft = left;
        wallUp = up;
        wallRight = right;
        wallBottom = down;
        position = new Position(x, y);
        hasTrap = false;
    }

    public bool IsExit(Position exitPosition)
    {
        return position.posX == exitPosition.posX && position.posY == exitPosition.posY;
    }
    public bool IsStartpoint(Position startPosition)
    {
        return position.posX == startPosition.posX && position.posY == startPosition.posY;
    }

    public void RemoveOppositeWall(EWallSide wall)
    {
        switch (wall)
        {
            case EWallSide.WALL_LEFT:
                wallRight = false;
                break;
            case EWallSide.WALL_UP:
                wallBottom = false;
                break;
            case EWallSide.WALL_RIGHT:
                wallLeft = false;
                break;
            case EWallSide.WALL_BOTTOM:
                wallUp = false;
                break;

        }
    }

    public void RemoveWall(EWallSide wall)
    {
        switch (wall)
        {
            case EWallSide.WALL_LEFT:
                wallLeft = false;
                break;
            case EWallSide.WALL_UP:
                wallUp = false;
                break;
            case EWallSide.WALL_RIGHT:
                wallRight = false;
                break;
            case EWallSide.WALL_BOTTOM:
                wallBottom = false;
                break;
        }
    }
}

