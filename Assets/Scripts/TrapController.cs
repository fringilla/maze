using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapController : MonoBehaviour
{
    [SerializeField]
    private Material trapMaterial;
    [SerializeField]
    private Material normalFloorMaterial;
    [SerializeField]
    private MeshRenderer mesh;
    [SerializeField]
    private float maxTrapTime = 3;
    [SerializeField]
    private float minTrapTime = 2;
    [SerializeField]
    private float minNormalFloorTime = 1;
    [SerializeField]
    private float maxNormalFloorTime = 2;
    private bool trapEnabled = false;
    private float timer;

    public bool TrapEnabled
    {
        get { return trapEnabled; }
    }

    // Start is called before the first frame update
    void Start()
    {
        trapEnabled = false;
        timer = GetRandomNormalFloorTime();
        mesh.sharedMaterial = normalFloorMaterial;
    }
    private float GetRandomNormalFloorTime()
    {
        return Random.Range(minNormalFloorTime, maxNormalFloorTime);
    }
    private float GetRandomTrapTime()
    {
        return Random.Range(minTrapTime, maxTrapTime);
    }
    // Update is called once per frame
    void Update()
    {
        timer -= Time.deltaTime;
        if (timer <= 0)
        {
            trapEnabled = !trapEnabled;
            timer = trapEnabled ? GetRandomTrapTime() : GetRandomNormalFloorTime();
            mesh.sharedMaterial = trapEnabled ? trapMaterial : normalFloorMaterial;

        }
    }
}
