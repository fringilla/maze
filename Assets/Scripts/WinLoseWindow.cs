using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class WinLoseWindow : MonoBehaviour
{
    [SerializeField]
    private GameObject winPanel;
    [SerializeField]
    private GameObject losePanel;
    [SerializeField]
    private Button buttonLose;
    [SerializeField]
    private Button buttonWin;


    public void ShowWinPanel(UnityAction buttonAction)
    {
        winPanel.SetActive(true);
        buttonWin.onClick.AddListener(buttonAction);
    }
    public void ShowLosePanel(UnityAction buttonAction)
    {
        losePanel.SetActive(true);
        buttonLose.onClick.AddListener(buttonAction);
    }
    public void HideEverything()
    {
        buttonLose.onClick.RemoveAllListeners();
        buttonWin.onClick.RemoveAllListeners();
        winPanel.SetActive(false);
        losePanel.SetActive(false);

    }
   
}
