using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class PauseWindow : MonoBehaviour
{
    [SerializeField]
    private GameObject pausePanel;
    [SerializeField]
    private Button resumeButton;
    [SerializeField]
    private Button exitButton;


    public void Show(UnityAction resume, UnityAction exit)
    {
        pausePanel.SetActive(true);
        resumeButton.onClick.AddListener(resume);
        resumeButton.onClick.AddListener(Hide);
        exitButton.onClick.AddListener(exit);
    }
    public void Hide()
    {
        pausePanel.SetActive(false);
        resumeButton.onClick.RemoveAllListeners();
        exitButton.onClick.RemoveAllListeners();
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
