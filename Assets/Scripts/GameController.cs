using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    [SerializeField]
    private MazeController mazeController;
    [SerializeField]
    private WinLoseWindow winLoseWindow;
    [SerializeField]
    private PauseWindow pauseWindow;

    private bool pauseWindowVisible = false;
    private static GameController _instance;
    public static GameController Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<GameController>();              
            }

            return _instance;
        }
    }
    
    private bool GamePaused => Time.timeScale == 0f;

    public bool gameFinished = false;

    // Start is called before the first frame update
    void Start()
    {
        StartGame();
    }

    public void GameOver()
    {
        gameFinished = true;
        PauseGame();
        winLoseWindow.ShowLosePanel(StartGame);
    }
    public void Win()
    {
        gameFinished = true;
        PauseGame();
        winLoseWindow.ShowWinPanel(StartGame);
    }
    public void StartGame()
    {
        gameFinished = false;
        ResumeGame();
        winLoseWindow.HideEverything();
        mazeController.Clear();
        mazeController.BuildMaze();
    }

    public void PauseGame()
    {
        Cursor.lockState = CursorLockMode.None;
        Time.timeScale = 0f;
    }
    public void ResumeGame()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Time.timeScale = 1f;
    }
    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape) && !gameFinished)
        {
            if(!GamePaused)
            {
                PauseGame();
                pauseWindow.Show(ResumeGame, Application.Quit);
            }
            else
            {
                ResumeGame();
                pauseWindow.Hide();
            }

        }

    }
}
