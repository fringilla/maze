using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRotation : MonoBehaviour
{
    [SerializeField]
    private Transform playerBody;
    [SerializeField]
    public float mouseSensitivity = 500;
    private float rotation = 0;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        var mouseX = Input.GetAxis("Mouse X") * Time.deltaTime * mouseSensitivity;
        var mouseY = Input.GetAxis("Mouse Y") * Time.deltaTime * mouseSensitivity;

        rotation -= mouseY;
        rotation = Mathf.Clamp(rotation, -90, 90);

        transform.localRotation = Quaternion.Euler(rotation, 0 , 0);

        playerBody.Rotate(Vector3.up * mouseX);
    }
    private void FixedUpdate()
    {

    }
}
