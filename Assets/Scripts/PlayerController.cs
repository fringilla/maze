﻿using UnityEngine;

class PlayerController : MonoBehaviour
{
    [SerializeField]
    private CharacterController controller;
    [SerializeField]
    private float normalSpeed = 1.3f;
    [SerializeField]
    private float fastSpeed = 2.5f;
    private float speed;
    private void Update()
    {

    }
    private void FixedUpdate()
    {
        var mouseX = Input.GetAxisRaw("Horizontal");
        var mouseY = Input.GetAxisRaw("Vertical");
        if (Input.GetKey(KeyCode.LeftShift) && mouseY != 0)
        {
            speed = fastSpeed;
        }
        else
        {
            speed = normalSpeed;
        }
        var move = transform.right * mouseX + transform.forward * mouseY;
        controller.Move(move * speed * Time.deltaTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Trap")
        {
            Trapped(other);
        }
        else if (other.tag == "Endpoint")
        {
            GameController.Instance.Win();
        }
    }
    private void Trapped(Collider other)
    {
        if (other.tag == "Trap")
        {
            if (other.GetComponent<TrapController>().TrapEnabled)
            {
                GameController.Instance.GameOver();
            }
        }
    }
    private void OnTriggerStay(Collider other)
    {
        Trapped(other);
    }
}
