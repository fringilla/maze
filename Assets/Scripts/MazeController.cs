using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MazeController : MonoBehaviour
{
    [Header("Maze settings")]
    [SerializeField]
    private int mazeWidth;
    [SerializeField]
    private int mazeHeight;
    [SerializeField]
    private Transform mazeParent;
    [SerializeField]
    private GameObject playerObj;
    [SerializeField]
    [Range(0, 1)]
    private float trapPropability = 0.1f;

    [Header("Walls")]
    [SerializeField]
    private GameObject wallPrefab;
    [SerializeField]
    private float wallThickness = 0.1f;
    [SerializeField]
    private float wallLength = 0.5f;
    [SerializeField]
    private float wallHeight = 1;
    [Header("Floors")]
    [SerializeField]
    private GameObject floorPrefab;
    [SerializeField]
    private GameObject trapFloorPrefab;
    [SerializeField]
    private GameObject endPointFloorPrefab;

    private Maze maze;

    private void Start()
    {

    }
    public void BuildMaze()
    {
        maze = MazeGenerator.GenerateMaze(mazeWidth, mazeHeight, trapPropability, 2);
        DrawMaze(maze);
    }
    private void DrawMaze(Maze maze)
    {
        for (int x = 0; x < mazeWidth; x++)
        {
            for (int y = 0; y < mazeHeight; y++)
            {
                MazeCell cell = maze.grid[x, y];
                var position = new Vector3(-mazeWidth / 2 + x * wallLength, 0, -mazeHeight / 2 + y * wallLength);
                var scale = new Vector3(wallThickness, wallHeight, wallLength);
                if (cell.wallLeft)
                {
                    var wallObj = Instantiate(wallPrefab, mazeParent);
                    wallObj.transform.position = position + new Vector3(-wallLength / 2, 0, 0);
                    wallObj.transform.localScale = scale;
                    wallObj.name = string.Format("Wall {0} ({1},{2})", "left", x, y);

                }
                if (cell.wallUp)
                {
                    var wallObj = Instantiate(wallPrefab, mazeParent);
                    wallObj.transform.position = position + new Vector3(0, 0, wallLength / 2);
                    wallObj.transform.rotation = Quaternion.Euler(0, 90, 0);
                    wallObj.transform.localScale = scale;
                    wallObj.name = string.Format("Wall {0} ({1},{2})", "up", x, y);
                }
                //right edge
                if (x == mazeWidth - 1)
                {
                    var wallObj = Instantiate(wallPrefab, mazeParent);
                    wallObj.transform.position = position + new Vector3(wallLength / 2, 0, 0);
                    wallObj.transform.localScale = scale;
                    wallObj.name = string.Format("Wall {0} ({1},{2})", "right", x, y);
                }
                //bottom edge
                if (y == 0)
                {
                    var wallObj = Instantiate(wallPrefab, mazeParent);
                    wallObj.transform.position = position + new Vector3(0, 0, -wallLength / 2);
                    wallObj.transform.rotation = Quaternion.Euler(0, 90, 0);
                    wallObj.transform.localScale = scale;
                    wallObj.name = string.Format("Wall {0} ({1},{2})", "down", x, y);
                }
                GameObject floorObj = null;

                if (cell.IsExit(maze.exitPosition))
                {
                    floorObj = Instantiate(endPointFloorPrefab, mazeParent);
                }
                else if (cell.hasTrap)
                {
                    floorObj = Instantiate(trapFloorPrefab, mazeParent);

                }
                else
                {
                    floorObj = Instantiate(floorPrefab, mazeParent);
                }

                floorObj.name = string.Format("Floor ({0},{1})", x, y);
                floorObj.transform.position = position + new Vector3(0, -wallHeight / 2);
                floorObj.transform.localScale = new Vector3(wallLength, wallLength, 1);
            }
        }
        
        var startPosition = new Vector3(-mazeWidth / 2 + maze.startPosition.posX * wallLength, 0, -mazeHeight / 2 + maze.startPosition.posY * wallLength);

        var start = Instantiate(playerObj, startPosition, Quaternion.identity, mazeParent);
        start.transform.SetAsFirstSibling();
    }


    public void Clear()
    {
        maze = null;
        foreach (Transform child in mazeParent)
        {
            Destroy(child.gameObject);
        }

    }

}
